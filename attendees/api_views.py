from django.urls import reverse
from django.http import JsonResponse
from .models import Attendee


def api_list_attendees(request, conference_id):
    # Query the database for the attendees for the specified conference ID
    attendees = Attendee.objects.filter(conference_id=conference_id)

    # Construct a list of dictionaries containing the name and URL for each attendee
    attendees_list = []
    for attendee in attendees:
        attendee_dict = {
            "name": attendee.name,
            "href": reverse("api_show_attendee", args=[attendee.id]),
        }
        attendees_list.append(attendee_dict)

    # Create a dictionary with a single key "attendees" that contains the list of attendees
    response_data = {"attendees": attendees_list}

    # Return a JsonResponse object with the response data
    return JsonResponse(response_data)


def api_show_attendee(request, id):
    # Retrieve the Attendee instance with the specified ID
    attendee = Attendee.objects.get(id=id)

    # Construct a dictionary with the details for the Attendee instance
    attendee_dict = {
        "email": attendee.email,
        "name": attendee.name,
        "company_name": attendee.company_name,
        "created": attendee.created,
        "conference": {
            "name": attendee.conference.name,
            "href": reverse(
                "api_list_conferences", args=[attendee.conference.id]
            ),
        },
    }
    # Return a JsonResponse object with the attendee details
    return JsonResponse(attendee_dict)
